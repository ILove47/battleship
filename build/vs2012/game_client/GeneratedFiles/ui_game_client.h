/********************************************************************************
** Form generated from reading UI file 'game_client.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAME_CLIENT_H
#define UI_GAME_CLIENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_game_clientClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *game_clientClass)
    {
        if (game_clientClass->objectName().isEmpty())
            game_clientClass->setObjectName(QStringLiteral("game_clientClass"));
        game_clientClass->resize(600, 400);
        menuBar = new QMenuBar(game_clientClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        game_clientClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(game_clientClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        game_clientClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(game_clientClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        game_clientClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(game_clientClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        game_clientClass->setStatusBar(statusBar);

        retranslateUi(game_clientClass);

        QMetaObject::connectSlotsByName(game_clientClass);
    } // setupUi

    void retranslateUi(QMainWindow *game_clientClass)
    {
        game_clientClass->setWindowTitle(QApplication::translate("game_clientClass", "game_client", 0));
    } // retranslateUi

};

namespace Ui {
    class game_clientClass: public Ui_game_clientClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAME_CLIENT_H
