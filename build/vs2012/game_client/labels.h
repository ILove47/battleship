#ifndef _BS_LABLES
#define _BS_LABLES
#include <string>
namespace battleship
{
	class CLables
	{
	public:
		static inline std::string get_invalid() { return ""; }
		static inline std::string get_hello() { return " Hello! "; }
		static inline std::string get_exit() { return " Game end! "; }
		static inline std::string get_preparations_for_game() { return " Prepare for battle! "; }
		static inline std::string get_your_move() { return " Your move. "; }
		static inline std::string get_his_move() { return " His move. "; }
		static inline std::string get_warning_ship() { return "Warning : You can't place ship here "; }
		static inline std::string get_warning_ship_rotate() { return "Warning : You can't rotate ship here "; }
		static inline std::string get_warning_shot() { return "Warning : You can't shot here "; }
		static inline std::string get_you_win() { return " You win! "; }
		static inline std::string get_computer_win() { return " Computer win =( "; }
	};
}
#endif // _BS_LABLES
