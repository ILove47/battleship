#ifndef _BS_BOARDVIEW
#define _BS_BOARDVIEW
#include <vector>
#include "board_model.h"
#include "canvas2d.h"
namespace battleship
{
	class CCellView
	{
	public:
		CCellView(const CCell & cell = CCell::get_empty_and_not_attacked());
		void draw(CCanvas2D& canvas, int x1, int y1, int x2, int y2);
		void set_cell(const CCell & cell);
	private:
		CCell m_cell;
	};
	class CBoardView;
	class CShipView
	{
	public:
		CShipView(int position_x,int position_y, const CShip& ship);
		void draw(CCanvas2D& canvas);
		void set_position_x(int position_x);
		void set_position_y(int position_y);
		void update_position(const CBoardView& board);
		CShip* get_ship() const;
		bool is_in(std::pair<int,int> position) const;
	private:
		CShip* m_ship;
		int m_position_x;
		int m_position_y;
		const static int m_cell_width = 30;
		const static int m_cell_height = 30;
	};
	typedef std::vector<CShipView> CShipViewContainer;
	typedef std::vector<CCellView> CCellsViewContainer;
	class CBoardView
	{
	public:
		CBoardView(int position_x, int position_y);
		void draw(CCanvas2D& canvas);
		void reset();
		void update_cells(const CCellsContainer * con);
		void set_rows(int rows);
		void set_columns(int columns);
		bool is_in(std::pair<int,int> position) const;
		std::pair<int,int> get_graphic_position(const CBoardPosition & position) const;
		CBoardPosition get_position_clicked(std::pair<int,int> position) const;
	private:
		const static int m_width = 300;
		const static int m_height = 300;
		int m_rows;
		int m_columns;
		int m_position_x;
		int m_position_y;
		CCellsViewContainer m_cells;
	};
}

#endif //_BS_BOARDVIEW