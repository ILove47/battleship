#ifndef DIALOG_H
#define DIALOG_H
#include <QTcpServer>

class Connection;
class CConnectionManager : public QTcpServer
{
    Q_OBJECT

public:
    CConnectionManager(QObject *parent = 0);

signals:
    void newConnection(Connection *connection);

protected:
    void incomingConnection(qintptr socketDescriptor);
};


#endif // DIALOG_H