#include "board_view.h"
namespace battleship
{
	CCellView::CCellView(const CCell & cell) : m_cell(cell)
	{
	}
	void CCellView::set_cell(const CCell & cell)
	{
		m_cell = cell;
	}
	void CCellView::draw(CCanvas2D& canvas, int x1, int y1, int x2, int y2)
	{
		if(m_cell.is_attacked() && m_cell.is_ship_on())
		{//shot ship
			canvas.set_pen_color(255,0,0);
			canvas.set_brush_color(255,0,0);
			canvas.draw_rectangle(x1,y1,x2,y2);
			canvas.fill_rectangle(x1,y1,x2,y2);
		}
		else if(m_cell.is_attacked() && m_cell.is_empty())
		{//missed
			canvas.set_pen_color(0,0,255);
			canvas.set_brush_color(0,0,255);
			canvas.draw_rectangle(x1,y1,x2,y2);
			canvas.fill_rectangle(x1,y1,x2,y2);
		}
		else if(!m_cell.is_attacked() && m_cell.is_ship_on())
		{//normal tratata
			canvas.set_pen_color(0,0,0);
			canvas.set_brush_color(0,0,0);
			canvas.draw_rectangle(x1,y1,x2,y2);
			canvas.fill_rectangle(x1,y1,x2,y2);
		}else
		{
			canvas.set_pen_color(0,0,0);
			canvas.draw_rectangle(x1,y1,x2,y2);
		}

	}
	CShipView::CShipView(int position_x,int position_y, const CShip& ship):m_position_x (position_x), m_position_y(position_y), m_ship(new CShip(ship))
	{
	}
	CShip* CShipView::get_ship() const
	{
		return m_ship;
	}
	void CShipView::draw(CCanvas2D& canvas)
	{
		canvas.begin_paint();

		canvas.set_brush_color(0,0,0);
		int len = m_ship->get_ship_type();
		if(m_ship->is_vertical())
		{
			canvas.fill_rectangle(m_position_x,m_position_y,m_cell_width,len*m_cell_height);
		}
		else
		{
			canvas.fill_rectangle(m_position_x ,m_position_y,len*m_cell_width,m_cell_height);
		}
		canvas.end_paint();
	}
	void CShipView::update_position(const CBoardView& board)
	{
		if(is_valid(m_ship->get_start_position()))
		{
			set_position_x(board.get_graphic_position(m_ship->get_start_position()).first);
			set_position_y(board.get_graphic_position(m_ship->get_start_position()).second);
		}
	}
	std::pair<int,int> CBoardView::get_graphic_position(const CBoardPosition & position) const
	{
		return std::make_pair(position.get_column() * (m_width/m_columns) + m_position_x, position.get_row() * (m_height/m_rows) + m_position_y);
	}
	void CShipView::set_position_x(int position_x)
	{
		m_position_x = position_x;
	}
	void CShipView::set_position_y(int position_y)
	{
		m_position_y = position_y;
	}
	CBoardView::CBoardView(int position_x, int position_y) : m_position_x(position_x), m_position_y(position_y)
	{
		m_rows = 10;
		m_columns = 10;
		for(int i=0;i<m_columns*m_rows;i++)
			m_cells.push_back(CCellView());
	}
	void CBoardView::update_cells(const CCellsContainer * con)
	{
		if(m_cells.size() == con->size())
		{
			auto it = m_cells.begin();
			for(auto &cell : *con)
			{
				*it++ = cell;
			}
		}
		else
		{
			m_cells.clear();
			for(auto &cell : *con)
			{
				m_cells.push_back(CCellView(cell));
			}
		}

	}
	void CBoardView::set_rows(int rows)
	{
		m_rows = rows;
	}
	void CBoardView::set_columns(int columns)
	{
		m_columns = columns;
	}
	void CBoardView::reset()
	{
		m_rows = 10;
		m_columns = 10;
		m_cells.clear();
		for(int i=0;i<m_columns*m_rows;i++)
			m_cells.push_back(CCellView());
	}
	void CBoardView::draw(CCanvas2D& canvas)
	{
		canvas.begin_paint();
		canvas.draw_rectangle(m_position_x,m_position_y,m_width, m_height);
		int number = 0;
		int cell_width = (m_width/m_columns);
		int cell_height = (m_height/m_rows);

		for(auto &cell : m_cells)
		{
			int position_x = m_position_x + cell_width * (number%m_columns);
			int position_y = m_position_y + cell_height * (number/m_rows);
			cell.draw(canvas, position_x, position_y, cell_width, cell_height);
			number++;
		}
		canvas.end_paint();
	}
	bool CShipView::is_in(std::pair<int,int> position) const
	{
		bool result = true;
		int len = m_ship->get_ship_type();
		int x0 = m_position_x;
		int y0 = m_position_y;
		int x1 = x0;
		int y1 = y0;
		if (m_ship->is_vertical())
		{
			y1 += len * m_cell_height;
			x1 += m_cell_width;
		}
		else
		{
			y1 += m_cell_height;
			x1 += len * m_cell_width;
		}
		if (position.first < x0 || position.first > x1 || position.second < y0 || position.second > y1)
			result = false;
		return result;
	}
	bool CBoardView::is_in(std::pair<int,int> position) const
	{
		bool result = false;
		if (this != NULL)
		{
			if(position.first >= m_position_x && position.first <= m_position_x + m_width &&
				position.second >= m_position_y && position.second <= m_position_y + m_height)
			{
				result = true;
			}
		}
		return result;
	}
	CBoardPosition CBoardView::get_position_clicked(std::pair<int,int> position) const
	{
		return CBoardPosition((position.second - m_position_y)/(m_height/m_columns),(position.first - m_position_x)/(m_width/m_rows));
	}
}