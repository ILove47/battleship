#ifndef _BS_ENEMYBOARD
#define _BS_ENEMYBOARD
#include "board_model.h"
namespace battleship 
{
	class CEnemyBoard : public CBoardModel
	{
	public:
		void get_all_cells(CCellsContainer& cells)
		{
			CCellsContainer data;
			CBoardModel::get_all_cells(data);
			for( auto& cell : data)
			{
				if(cell.is_ship_on() && !cell.is_attacked())
				{
					cell.remove_ship();
				}
			}
			cells.swap(data);
		}
	};
}

#endif // _BS_ENEMYBOARD