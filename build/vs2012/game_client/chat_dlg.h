

#ifndef CHATDIALOG_H
#define CHATDIALOG_H

#include "ui_chatdialog.h"
#include "game_client.h"
#include "engine.h"
class ChatDialog : public QDialog, private Ui::ChatDialog
{
	Q_OBJECT

public:
	ChatDialog(battleship::Engine* engine, QWidget *parent = 0);
	void sendSetShip(const QString& ships);
	void sendSetMove(const QString& move);

public slots:
	void appendMessage(const QString &from, const QString &message);
	void appendInvite(const QString &from, const QString &message);
	void appendAccept(const QString &fake, const QString &name);
	void SetGameShips(const QString &fake, const QString &ships);
	void MakeMove(const QString &fake, const QString &move);

private slots:
	void returnPressed();
	void invitePressed();
	void onListItemClick(QListWidgetItem * item);
	void newParticipant(const QString &nick);
	void participantLeft(const QString &nick);


private:
	QString get_focused_player();
	Client client;
	QString myNickName;
	QTextTableFormat tableFormat;
	battleship::Engine* _engine;
};

#endif
