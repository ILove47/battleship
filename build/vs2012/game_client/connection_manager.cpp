#include <QtGui>
#include <QDebug>
#include <QtNetwork>
#include "connection.h"
#include <QtWidgets>
#include "connection_manager.h"
#include "input_source.h"
#include "chat_dlg.h"


ChatDialog::ChatDialog(battleship::Engine* engine, QWidget *parent)
    : QDialog(parent), _engine(engine)
{
    setupUi(this);
	_engine->get_game()->set_dialog(this);
	_engine->get_game()->set_network_game();
    lineEdit->setFocusPolicy(Qt::StrongFocus);
    textEdit->setFocusPolicy(Qt::NoFocus);
    textEdit->setReadOnly(true);
    listWidget->setFocusPolicy(Qt::NoFocus);

    connect(lineEdit, SIGNAL(returnPressed()), this, SLOT(returnPressed()));
    connect(lineEdit, SIGNAL(returnPressed()), this, SLOT(returnPressed()));
	connect(listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(onListItemClick(QListWidgetItem*)));
	connect(invitebutton, SIGNAL(clicked()), this, SLOT(invitePressed()));
	connect(&client, SIGNAL(newMessage(QString,QString)),this, SLOT(appendMessage(QString,QString)));
	connect(&client, SIGNAL(newInvite(QString,QString)), this, SLOT(appendInvite(QString,QString)));
	connect(&client, SIGNAL(newAccept(QString,QString)), this, SLOT(appendAccept(QString,QString)));
	connect(&client, SIGNAL(getShips(QString,QString)),  this, SLOT(SetGameShips(QString,QString)));
	connect(&client, SIGNAL(getMove(QString,QString)),   this, SLOT(MakeMove(QString,QString)));
	
    connect(&client, SIGNAL(newParticipant(QString)),    this, SLOT(newParticipant(QString)));
    connect(&client, SIGNAL(participantLeft(QString)),   this, SLOT(participantLeft(QString)));

    myNickName = client.nickName();
    newParticipant(myNickName);
    tableFormat.setBorder(0);
}

void ChatDialog::appendMessage(const QString &from, const QString &message)
{
    if (from.isEmpty() || message.isEmpty())
        return;

    QTextCursor cursor(textEdit->textCursor());
    cursor.movePosition(QTextCursor::End);
    QTextTable *table = cursor.insertTable(1, 2, tableFormat);
    table->cellAt(0, 0).firstCursorPosition().insertText('<' + from + "> ");
    table->cellAt(0, 1).firstCursorPosition().insertText(message);
    QScrollBar *bar = textEdit->verticalScrollBar();
	//	_engine->get_game()->
    bar->setValue(bar->maximum());
}
void ChatDialog::sendSetShip(const QString& ships)
{
	client.sendSetShipMessage(ships);
}
void ChatDialog::sendSetMove(const QString& move)
{
	client.sendMoveMessage(move);
}
void ChatDialog::SetGameShips(const QString &fake, const QString &ships)
{
	_engine->get_game()->set_opponent_ships(ships);
}

void ChatDialog::MakeMove(const QString &fake, const QString& move)
{
	_engine->get_game()->set_opponent_move(move);
}

void ChatDialog::appendInvite(const QString &from, const QString &message)
{
	for(int i=0; i<listWidget->count(); i++)
	{
		if(listWidget->item(i)->text() == message)
		{
			listWidget->item(i)->setTextColor(QColor(0,0,100));
			listWidget->update();
		}
	}
	_engine->get_game()->firstMove(false);
}
void ChatDialog::appendAccept(const QString &fake, const QString &name)
{
	_engine->get_game()->add_event(new battleship::ControlEvent(battleship::ControlEvent::Type_Start_App));
	_engine->get_game()->firstMove(true);
}

void ChatDialog::returnPressed()
{
    QString text = lineEdit->text();
    if (text.isEmpty())
        return;

    if (text.startsWith(QChar('/'))) {
        QColor color = textEdit->textColor();
        textEdit->setTextColor(Qt::red);
        textEdit->append(tr("! Unknown command: %1")
                         .arg(text.left(text.indexOf(' '))));
        textEdit->setTextColor(color);
    } else {
        client.sendMessage(text);
        appendMessage(myNickName, text);
    }

    lineEdit->clear();
}
QString ChatDialog::get_focused_player()
{
	return listWidget->currentItem()->text();
}
void ChatDialog::onListItemClick(QListWidgetItem * item)
{
	client.setOpponentName(item->text());
	invitebutton->setDisabled(false);
	if(item->textColor() == QColor(0,0,100))
	{
		invitebutton->setText("Accept");
	}
}
void ChatDialog::invitePressed()
{
	QString name = get_focused_player();
	if(invitebutton->text() == "Invite")
	{
		client.sendInviteMessage();
	}
	else
	{
		client.sendAcceptMessage();
		_engine->get_game()->add_event(new battleship::ControlEvent(battleship::ControlEvent::Type_Start_App));
	}
}
void ChatDialog::newParticipant(const QString &nick)
{
    if (nick.isEmpty())
        return;

    QColor color = textEdit->textColor();
    textEdit->setTextColor(Qt::gray);
    textEdit->append(tr("* %1 has joined").arg(nick));
    textEdit->setTextColor(color);
    listWidget->addItem(nick);
}

void ChatDialog::participantLeft(const QString &nick)
{
    if (nick.isEmpty())
        return;

    QList<QListWidgetItem *> items = listWidget->findItems(nick,Qt::MatchExactly);
    if (items.isEmpty())
        return;

    delete items.at(0);
    QColor color = textEdit->textColor();
    textEdit->setTextColor(Qt::gray);
    textEdit->append(tr("* %1 has left").arg(nick));
    textEdit->setTextColor(color);
}

CConnectionManager::CConnectionManager(QObject *parent)
    : QTcpServer(parent)
{
    listen(QHostAddress::Any);
}
void CConnectionManager::incomingConnection(qintptr socketDescriptor)
{
    Connection *connection = new Connection(this);
    connection->setSocketDescriptor(socketDescriptor);
    emit newConnection(connection);
}