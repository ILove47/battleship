#ifndef BS_ENGINE_H
#define BS_ENGINE_H

#include "engine_window.h"
#include "game.h"
namespace battleship
{

    // Represents platform for game. Encapsulates input, output, timers, etc.. 
    // This one, EngineWindow and Canvas2D are components that have to be implemented 
    // for each new platform.
    class Engine
    {
    public:
        Engine();
        virtual ~Engine();

        void run(CGame* game);
        EngineWindow* init_window();
		CViewInformation* update_game();
		Engine* get_engine();
		CGame* get_game();
    private:
        virtual EngineWindow* do_init_window() = 0;
        virtual void do_run() = 0;

    private:
        Engine(const Engine&);
        Engine& operator=(const Engine&);

        static Engine& instance();
      
        CGame* _game;        
    }; 
}

#endif // BS_ENGINE_H
