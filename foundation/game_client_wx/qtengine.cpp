#include <cassert>
#include <iostream>

#include "qtengine.h"
#include "qtengine_window.h"


namespace battleship 
{

    QtEngine::QtEngine(int argc, char* argv[])        
        : _app(new QApplication(argc, argv))
        , _window(new QtEngineWindow())
        , _timer(new QTimer())
		, _network_dlg(0)
    {        
        _timer->setSingleShot(false);
        connect(_timer, SIGNAL(timeout()), this, SLOT(idleFunc()));
    }

    void QtEngine::idleFunc()
    {    
		CViewInformation * info = update_game();
		if(info != NULL)
		{
			_window->update_view(info);
			delete info->get_board_data_player();
			delete info->get_board_data_enemy();
			delete info;
		}
		if(_window->is_network_game() && _network_dlg == NULL)
		{
			initialize_network();
		}
		
    }

    QtEngine::~QtEngine()
    {
        if (_window)
        {
            delete _window;
        }
        if (_app)
        {
            delete _app;
        }
        if (_timer)
        {
            delete _timer;
        }
    }
    void QtEngine::initialize_network()
	{
		_network_dlg = new ChatDialog(get_engine());
		_network_dlg->show();
	}
    EngineWindow* QtEngine::do_init_window()
    {
        // create and return (requires changes in protocol)platform specific window
        return _window;
    }

    void QtEngine::do_run()
    {        
        assert(_window != nullptr);
        assert(_timer != nullptr);
        _window->show();
        _timer->start(1);
        _app->exec();        
    }

}