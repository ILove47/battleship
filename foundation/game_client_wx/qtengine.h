#ifndef CH_QTENGINE_H
#define CH_QTENGINE_H

#include <QtWidgets/QApplication>
#include <QtCore/QTimer>

#include "engine.h"
#include "qtengine_window.h"
#include "chat_dlg.h"

namespace battleship 
{
    class QtEngine: public QObject, public Engine
    {
        Q_OBJECT
    public:
        QtEngine(int argc, char* argv[]);
        ~QtEngine();

    
    private:
        virtual EngineWindow* do_init_window();
        virtual void do_run();
		void initialize_network();
    
    private slots:
        void idleFunc();


    private:
        QApplication* _app;
        QtEngineWindow* _window;
        QTimer* _timer;
		ChatDialog* _network_dlg;
    };
}



#endif // CH_QTENGINE_H
