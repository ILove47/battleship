#include <cassert>

#include "engine.h"


namespace battleship
{

    Engine::Engine()
    {

    }
    
    Engine::~Engine()
    {
		delete _game;
    }

    void Engine::run(CGame* game)
    {
		_game = game;
        do_run();                
    }
	Engine* Engine::get_engine()
    {
		return this;
	}
	CGame* Engine::get_game()
    {
		return _game;
	}
    EngineWindow* Engine::init_window()
    {
        return do_init_window();
    }
	CViewInformation * Engine::update_game()
	{
		return _game->update();
	}


}