#include "game.h"
#include "canvas2d.h"
#include "chat_dlg.h"
namespace battleship
{    

	GameManager::GameManager(CPlayerBoard* player_board, CEnemyBoard* enemy_board) 
		:	m_player_board(player_board),
		m_enemy_board(enemy_board),
		m_first_attack_now(rand()%2),
		m_winer(0)
	{

	}
	GameManager::MoveType GameManager::make_move(CBoardPosition& pos)
	{
		MoveType result = GameManager::MoveType_Bad;
		if(is_first_go())
		{
			if(m_enemy_board->is_move_allowed(pos))
			{
				if (m_enemy_board->cell_at(pos).is_empty())
				{
					result = GameManager::MoveType_Missed;
				}
				else
				{
					result = GameManager::MoveType_Shot;
				}
				m_enemy_board->make_move(pos);
				m_first_attack_now = false;
			}
			if (result == GameManager::MoveType_Shot)
			{
				m_enemy_board->adjust_missed_cells(pos);
				m_first_attack_now = true;
			}
		}
		else
		{
			if(m_player_board->is_move_allowed(pos))
			{
				if (m_player_board->cell_at(pos).is_empty())
				{
					result = GameManager::MoveType_Missed;
				}
				else
				{
					result = GameManager::MoveType_Shot;
				}
				m_player_board->make_move(pos);
				m_first_attack_now = true;
			}
			if (result == GameManager::MoveType_Shot)
			{
				m_player_board->adjust_missed_cells(pos);
				m_first_attack_now = false;
			}
		}
		return result;
	}
	bool GameManager::is_first_go()
	{
		return m_first_attack_now;
	}
	void GameManager::set_first_move(bool b)
	{
		m_first_attack_now = b;
	}
	bool GameManager::is_game_finished()
	{
		bool result_player = true;
		bool result_enemy = true;
		CBoardPositionContainer con;
		m_enemy_board->get_all_available_moves(con);
		for(auto & pos : con)
		{
			if(m_enemy_board->cell_at(pos).is_ship_on() && !m_enemy_board->cell_at(pos).is_attacked())
			{
				result_player = false;
			}
		}
		m_player_board->get_all_available_moves(con);
		for(auto & pos : con)
		{
			if(m_player_board->cell_at(pos).is_ship_on() && !m_player_board->cell_at(pos).is_attacked())
			{
				result_enemy = false;
			}
		}
		if(result_enemy && !result_player)
		{
			m_winer = 1;
		}
		if(!result_enemy && result_player)
		{
			m_winer = 2;
		}
		return result_player || result_enemy;
	}
	std::string GameManager::get_winer() const
	{
		assert(m_winer);
		std::string result;
		if(m_winer == 2)
		{
			result = CLables::get_you_win();
		}
		if(m_winer == 1)
		{
			result = CLables::get_computer_win();
		}
		return result;
	}

	CGame::CGame(InputSource& input) : m_input_events(input), m_player_board(), m_enemy_board(), m_manager(&m_player_board,&m_enemy_board), m_game_start(false), m_end_game(false), m_app_start(false), network_game(false), m_opponent_move(NULL), m_opponent_ships(NULL), m_new_game(false)
	{	}

	CGame::~CGame()
	{
	}
	void CGame::start_app(CViewInformation * info)
	{
				m_player_board.resetBoard();
				m_enemy_board.resetBoard();
				m_app_start = true;
				m_end_game = false;
				m_game_start = false;
				info->set_new_game(!m_new_game);
				m_new_game = true;
				info->set_label(std::make_pair(true,CLables::get_preparations_for_game()));
	}
	void CGame::game_events(CViewInformation * info)
	{
		ControlEvent* _event = m_input_events.get_event();
		if(m_manager.is_game_finished())
		{
			info->set_label(std::make_pair(true,m_manager.get_winer()));
			m_game_start = false;
			m_end_game = true;
		}
		if(m_manager.is_first_go())
		{
			info->set_label(std::make_pair(true,CLables::get_your_move()));
		}
		else
		{
			info->set_label(std::make_pair(true,CLables::get_his_move()));
		}
		if(!m_manager.is_first_go())
		{
			if (!network_game)
			{
				CBoardPositionContainer moves;
				m_player_board.get_all_available_moves(moves);
				if(m_manager.make_move(CBot::get_move(moves)) == GameManager::MoveType_Bad)
				{
					assert(0);
					info->set_label(std::make_pair(false,CLables::get_warning_shot()));
				}
			}else
			{
				if (m_opponent_move != NULL)
				{
					if(m_manager.make_move(*m_opponent_move) == GameManager::MoveType_Bad)
					{
						info->set_label(std::make_pair(false,CLables::get_warning_shot()));
					}
					m_opponent_move = NULL;
				}else
				{
					switch (_event->type())
					{

					case ControlEvent::Type_Start_App:
						{
							start_app(info);
						}
						break;	
					default:
						{
							m_input_events.clear_pending_events();
							m_input_events.add_event(new ControlEvent(ControlEvent::Type_Invalid));
						}
						break;
					}
				}

			}
		}
		switch (_event->type())
		{
		case ControlEvent::Type_Start_App:
			{
				start_app(info);
			}
			break;
		case ControlEvent::Type_Board_Click:
			if(m_manager.is_first_go() && ((BoardEvent*)(_event))->type() == BoardEvent::BoardType_Enemy_Board)
			{
				CBoardPosition position = ((BoardEvent*)(_event))->get_pos();
				if(m_manager.make_move(position) == GameManager::MoveType_Bad)
				{
					info->set_label(std::make_pair(false,CLables::get_warning_shot()));
				}else
				{
					if(network_game)
					{
						m_dialog->sendSetMove(position.getQStringFromPosition());
					}
					else
					{
						if(m_manager.is_first_go() && ((BoardEvent*)(_event))->type() == BoardEvent::BoardType_Enemy_Board)
						{
							CBoardPosition position = ((BoardEvent*)(_event))->get_pos();
							if(m_manager.make_move(position) == GameManager::MoveType_Bad)
							{
								info->set_label(std::make_pair(false,CLables::get_warning_shot()));
							}
						}

					}
				}
			}
			while(_event->type() == ControlEvent::Type_Board_Click)
			{
				_event = m_input_events.get_event();
			}
			break;
		default:
			break;

		}
	}
	void CGame::set_opponent_ships(const QString& ships)
	{
		m_opponent_ships = new CEnemyBoard();
		for (int i = 0; i < ships.size(); ++i)
			if (ships[i]=='1')
			m_opponent_ships->cell_at(CBoardPosition(i/10,i%10)).put_ship_on();
	}
	void CGame::set_opponent_move(const QString& move)
	{
		int row = 0;
		int col = 0;
		int pos = 0;
		int len = move.size();
		while (move[pos] != '@')
		{
			row *= 10;
			row += move[pos++].digitValue();
		}
		for (int i = pos+1; i < len; ++i)
		{
			col *= 10;
			col += move[i].digitValue();
		}
		m_opponent_move = new CBoardPosition(row, col);
	}
	void CGame::set_network_game()
	{
		network_game = true;
	}
	void CGame::add_event(ControlEvent* _event)
	{
		m_input_events.add_event(_event);
	}
	void CGame::set_dialog(ChatDialog* dialog)
	{
		m_dialog = dialog;
	}
	void CGame::firstMove(bool b)
	{
		m_manager.set_first_move(b);
	}
	CViewInformation * CGame::update()
	{
		bool redraw_ships = false;
		CViewInformation * info = NULL;
		CCellsContainer player_cells;
		m_player_board.get_all_cells(player_cells);
		CCellsContainer enemy_cells;
		m_enemy_board.get_all_cells(enemy_cells);
		info = new CViewInformation(new CCellsContainer(enemy_cells), new CCellsContainer(player_cells));

		if(m_game_start)
		{
			game_events(info);
		}
		else 
		{
			if (m_end_game)
			{
				info->set_label(std::make_pair(true,m_manager.get_winer()));
				m_app_start = false;
			}

			ControlEvent* _event = m_input_events.get_event();
			switch (_event->type())
			{
			case ControlEvent::Type_Start_App:
				{
					start_app(info);
				}
				break;
			case ControlEvent::Type_Ship_Move:
				{
					if (m_app_start)
					{
						ControlEvent* second_event = m_input_events.get_event();
						switch (second_event->type())
						{
						case ControlEvent::Type_Invalid:
							{
								m_input_events.add_event(_event);
							}
							break;
						case ControlEvent::Type_Board_Click:
							{
								CShip* ship = ((ShipRotateEvent*)(_event))->get_ship();
								CBoardPosition pos = ship->get_start_position(); 
								CBoardPosition pos2 = ((BoardEvent*)(second_event))->get_pos();
								m_player_board.remove_ship(*ship);
								ship->set_start_position(pos2);
								if (m_player_board.is_add_ship_allowed(*ship))
								{
									m_player_board.add_ship(*ship);
									redraw_ships = true;

								}
								else
								{
									ship->set_start_position(pos);
									if(is_valid(ship->get_start_position()))
										m_player_board.add_ship(*ship);
									info->set_label(std::make_pair(false,CLables::get_warning_ship()));
								}
							}
							break;
						default:
							break;
						}
					}
				}
				break;
			case ControlEvent::Type_Ship_Rotate:
				{
					if (m_app_start)
					{
						CShip* ship = ((ShipRotateEvent*)(_event))->get_ship();
						m_player_board.remove_ship(*ship);
						ship->rotate();
						if (m_player_board.is_add_ship_allowed(*ship))
						{
							m_player_board.add_ship(*ship);
							redraw_ships = true;
						}
						else
						{
							ship->rotate();
							if(is_valid(ship->get_start_position()))
								m_player_board.add_ship(*ship);
							info->set_label(std::make_pair(false,CLables::get_warning_ship_rotate()));
						}
					}
				}
				break;
			case ControlEvent::Type_Get_Random_Ships:
				{
					if (m_app_start)
					{
						CShipPtrContainer* ships = ((GetRandomShipsEvent*)(_event))->get_ships();
						CShipPtrContainer* shipsOld = ships; 
						CBoardModel random_board;
						CShipContainer rand_ships = random_board.get_randomized_ships();
						int n = ships->size();
						m_player_board.resetBoard();
						for (int i = 0; i < n; ++i)
						{
							*(*ships)[i] = CShip(rand_ships[i]);
							m_player_board.add_ship(rand_ships[i]);
						}
						redraw_ships = true;
					}
				}
				break;
			case ControlEvent::Type_Start_Game:
				if (m_app_start)
				{
					m_game_start = true;
					if (network_game)
					{
						m_dialog->sendSetShip(m_player_board.get_all_cells_to_QString());
						if (m_opponent_ships != NULL)
						{
							m_enemy_board = *m_opponent_ships;
						}else
						{
							m_input_events.clear_pending_events();
							m_input_events.add_event(new ControlEvent(ControlEvent::Type_Start_Game));
							m_game_start = false;
						}

					}else
					{
						m_enemy_board.set_randomized_ships();
					}
					
				}
				break;
			default:
				break;
			}
		}
		info->set_update_ships(redraw_ships);
		return info;
	}

}