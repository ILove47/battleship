#ifndef CGameServer_H
#define CGameServer_H

#include <QTcpServer>
#include <QDebug>
#include <QtWidgets>
#include "game_client.h"
class CGameClient;

class CGameServer : public QTcpServer
{
    Q_OBJECT

public:
    explicit CGameServer(QWidget *widget = 0, QObject *parent = 0);
    bool doStartServer(qint16 port);
    void doSendToAllUserJoin(QString name);
    void doSendToAllUserLeft(QString name);
    void doSendToAllMessage(QString message, QString fromUsername);
    void doSendToAllServerMessage(QString message);
    void doSendServerMessageToUsers(QString message, const QStringList &users);
    void doSendMessageToUsers(QString message, const QStringList &users, QString fromUsername);
    QStringList getUsersOnline() const;
    bool isNameValid(QString name) const;
    bool isNameUsed(QString name) const;

signals:
    void addLogToGui(QString string, QColor color = Qt::black);

public slots:
    void onMessageFromGui(QString message, const QStringList &users);
    void onRemoveUser(CGameClient *client);

protected:
    void incomingConnection(int handle);

private:
    QList<CGameClient *> _clients;
    QWidget *_widget;

};

#endif // CGameServer_H
