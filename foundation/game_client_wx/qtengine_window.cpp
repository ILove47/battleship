#include <cassert>
#include <iostream>

#include <QtGui/QMouseEvent>
#include <QtGui/QKeyEvent>
#include <QtGui/QPainter>
#include "qtengine_window.h"



namespace battleship
{

    QtEngineWindow::QtEngineWindow()
        : ui(new Ui::game_clientClass)
        , newgame_action(0)
        , game_menu(0)
        , _canvasbuff(NULL)
        , _canvas(NULL)
        , _inframe(false)
		, player_board(0)
		, enemy_board(0)
		, ships()
		, start_button(0)
		, rand_button(0)
		, label()
		, label_warning()
		, network_game(false)
    {        
        reinit_canvas();
        create_actions();
        create_menus();
		create_labels();
        setWindowTitle("Battleship");
        resize(800,500);
    }

    void QtEngineWindow::create_actions()
    {
        newgame_action = new QAction("With computer", this);
        exit_action = new QAction("Exit", this);
		network_action = new QAction("Network game", this);
		connect(newgame_action, SIGNAL(triggered()), SLOT(on_new_game()));
        connect(exit_action, SIGNAL(triggered()), SLOT(on_exit_game()));
		connect(network_action, SIGNAL(triggered()), SLOT(on_network_game()));
    }
	void QtEngineWindow::create_labels()
	{
		label = new QLabel(this);
		label->setGeometry(320,380,180,20);
		label->setAlignment(Qt::AlignCenter);
		label->setText(CLables::get_hello().c_str());
		label->setVisible(true);
		label->update();
		label_warning = new QLabel(this);
		label_warning->setGeometry(320,400,180,20);
		label_warning->setAlignment(Qt::AlignCenter);
		label_warning->setText("");
		label_warning->setVisible(true);
		label_warning->update();
	}
	bool QtEngineWindow::is_all_ships_on_board()
	{
		bool result = true;
		auto it = ships.begin();
		auto end = ships.end();
		if(it==end)
			result = false;
		for(;it!=end;++it)
		{
			if(!is_valid(it->get_ship()->get_start_position()))
			{
				result = false;
			}
		}
		return result;
	}
    void QtEngineWindow::create_menus()
    {
        game_menu = menuBar()->addMenu(tr("&Game"));
        game_menu->addAction(newgame_action);
        game_menu->addSeparator();
		game_menu->addAction(network_action);
		game_menu->addSeparator();
        game_menu->addAction(exit_action);
    }

    QtEngineWindow::~QtEngineWindow()
    {
        delete_objects();
    }

    void QtEngineWindow::delete_objects()
    {
        if (_canvas)
        {
            delete _canvas;
            _canvas = NULL;
        }
        if (_canvasbuff)
        {
            delete _canvasbuff;
            _canvasbuff = NULL;
        }
    }

    CCanvas2D& QtEngineWindow::do_get_canvas()
    {
        if (!_canvas)
        {
            reinit_canvas();
        }
        return *_canvas;
    }

    void QtEngineWindow::do_begin_frame()
    {
        _canvas->begin_paint(); 
    }

    void QtEngineWindow::do_end_frame()
    {
        _canvas->end_paint(); // this will cause sending us paintEvent        
    }

    void QtEngineWindow::reinit_canvas()
    {
        delete _canvas;
        _canvas = new QtCanvas2D(this);
        reinit_canvasbuffer();
        _canvas->attach_buffer(_canvasbuff);
    }

    void QtEngineWindow::reinit_canvasbuffer()
    {
        delete _canvasbuff;
        _canvasbuff = new QPixmap(QMainWindow::size());
    }

    void QtEngineWindow::paintEvent(QPaintEvent *event)
    {
        QPainter painter;
        painter.begin(this);
        painter.setRenderHint(QPainter::Antialiasing);        
        painter.drawPixmap(0,0, *_canvasbuff);
        painter.end();
    }

    void QtEngineWindow::resizeEvent(QResizeEvent *event)
    {        
        _canvas->detach_buffer();
        reinit_canvasbuffer();
        _canvas->attach_buffer(_canvasbuff);
    }

    void QtEngineWindow::mousePressEvent(QMouseEvent *event)
    {        
		std::pair<int, int> position = std::make_pair(event->x(), event->y());
		bool left_click = event->button() == Qt::LeftButton;
		bool right_click = event->button() == Qt::RightButton;

		bool b = true;
		{
			CShipViewContainer::iterator it = ships.begin();
			CShipViewContainer::iterator itEnd = ships.end();
			for (;it != itEnd; ++it)
			{
				if (it->is_in(position))
				{
					if (left_click)
					{
						ShipMoveEvent sevent;
						sevent.set_ship(it->get_ship());
						ControlEvent* ship_event(new ShipMoveEvent(sevent));
						_input.add_event(ship_event);
						b = false;
						break;
					}else if (right_click)
					{
						ShipRotateEvent sevent;
						sevent.set_ship(it->get_ship());
						ControlEvent* ship_event(new ShipRotateEvent(sevent));
						_input.add_event(ship_event);
						b = false;
						break;
					}
				}
			}
		}
		if (b)
		{
			if (enemy_board->is_in(position))
			{
				BoardEvent bevent;
					bevent.set_type(BoardEvent::BoardType_Enemy_Board);
				bevent.set_pos(enemy_board->get_position_clicked(position));
				ControlEvent* board_event(new BoardEvent(bevent));
				_input.add_event(board_event);
			}else if (player_board->is_in(position))
			{
				BoardEvent bevent;
				bevent.set_type(BoardEvent::BoardType_Enemy_Board);
				bevent.set_pos(player_board->get_position_clicked(position));
				ControlEvent* board_event(new BoardEvent(bevent));
				_input.add_event(board_event);
			}
		}
		label_warning->setText("");
		label_warning->update();

    }

    InputSource& QtEngineWindow::do_get_input()
    {
        return _input;
    }
	void QtEngineWindow::intialize_buttons()
	{
		start_button = new QPushButton("Start",this);
		start_button->setGeometry(380,140,50,30);
		start_button->setVisible(true);
		start_button->setDisabled(true);
		start_button->update();
		rand_button = new QPushButton("Rand",this);
		rand_button->setGeometry(380,220,50,30);
		rand_button->setVisible(true);
		rand_button->update();
		
		connect(start_button, SIGNAL(clicked()), this, SLOT(on_start_game()));

		connect(rand_button, SIGNAL(clicked()), this, SLOT(on_rand_ships()));
	}
    void QtEngineWindow::on_new_game()
    {
		player_board = new CBoardView(50,50);
		enemy_board = new CBoardView(450,50);
		_input.add_event(new ControlEvent(ControlEvent::Type_Start_App));
		ships.clear();
		
		int bposy = 360;
		int bposx = 50;
		intialize_buttons();
		for(int i=0;i<4;i++)
		{
			ships.push_back(CShipView(bposx, bposy, CShip::ShipType_Single_Decker));
		}
		for(int i=0;i<3;i++)
		{
			ships.push_back(CShipView(bposx + 40, bposy, CShip::ShipType_Two_Decker));
		}
		for(int i=0;i<2;i++)
		{
			ships.push_back(CShipView(bposx + 80, bposy, CShip::ShipType_Three_Decker));
		}
		for(int i=0;i<1;i++)
		{
			ships.push_back(CShipView(bposx + 120, bposy, CShip::ShipType_Four_Decker));
		}

		for (int i = 0; i < ships.size(); ++i)
			ships[i].draw(*_canvas);
		player_board->draw(*_canvas);
		enemy_board->draw(*_canvas);
    }
    
    void QtEngineWindow::on_exit_game()
    {
		std::cout<<"exit"<<std::endl;
		close();
    }
	void QtEngineWindow::on_start_game()
	{
		ships.clear();
		_input.add_event(new ControlEvent(ControlEvent::Type_Start_Game));
	}
	void QtEngineWindow::on_rand_ships()
	{
		CShipPtrContainer* shipss = new CShipPtrContainer();
		for (auto& ship:ships)
		{
			shipss->push_back(ship.get_ship());
		}
		_input.add_event(new GetRandomShipsEvent(shipss));
	}
	bool QtEngineWindow::is_network_game()
	{
		return network_game;
	}
	void QtEngineWindow::on_network_game()
	{
		network_game = true;
	}
	void QtEngineWindow::update_view(CViewInformation * info)
	{
		if (info->is_new_game())
		{
			on_new_game();
		}
		reinit_canvas();
		update_boards(info);
		update_ships(info);
		if(is_all_ships_on_board() && start_button)
		{
			start_button->setDisabled(false);
		}
		if(info->get_label() != CLables::get_invalid())
		{
			if(info->is_main_lable_modified())
			{
				label->setText(info->get_label().c_str());
				label->update();
			}
			else
			{
				label_warning->setText(info->get_label().c_str());
				label_warning->update();
			}
		}
	}
	void QtEngineWindow::update_boards(CViewInformation * info)
	{
		if(player_board != NULL)
		{
			player_board->update_cells(info->get_board_data_player());
			enemy_board->update_cells(info->get_board_data_enemy());
			
			player_board->draw(*_canvas);
			enemy_board->draw(*_canvas);
		}
	}
	void QtEngineWindow::update_ships(CViewInformation * info)
	{
			CShipViewContainer::iterator it = ships.begin();
			CShipViewContainer::iterator itEnd = ships.end();
			for (; it != itEnd; ++it)
			{
				it->update_position(*player_board);
				it->draw(*_canvas);
			}
	}


}