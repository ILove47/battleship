#ifndef BS_GAME_H
#define BS_GAME_H
#include "input_source.h"
#include "player_board.h"
#include "enemy_board.h"
#include "bot.h"


	class ChatDialog;
namespace battleship
{ 
	class GameManager
	{
	public:
		enum MoveType {MoveType_Bad, MoveType_Missed, MoveType_Shot};
		GameManager(CPlayerBoard* player_board, CEnemyBoard* enemy_board);
		MoveType make_move(CBoardPosition& pos);
		bool is_first_go();
		bool is_game_finished();
		void set_first_move(bool b);
		std::string get_winer() const;
	private:
		CPlayerBoard* m_player_board;
		CEnemyBoard* m_enemy_board;
		bool m_first_attack_now;
		int m_winer;
	};
	class CGame
    {
    public:
		CGame(InputSource& input);
        ~CGame();
		CViewInformation * update();
		void game_events(CViewInformation * info);
		void set_opponent_ships(const QString& ships);
		void set_opponent_move(const QString& move);
		void set_network_game();
		void add_event(ControlEvent* _event);
		void set_dialog(ChatDialog* dialog);
		void firstMove(bool b);
	private: 
		void start_app(CViewInformation * info);
	private:
		//CCanvas2D& m_canvas;
		InputSource& m_input_events;
		CPlayerBoard m_player_board;
		CEnemyBoard m_enemy_board;
		GameManager m_manager;
		ChatDialog* m_dialog;
		CEnemyBoard* m_opponent_ships;
		CBoardPosition* m_opponent_move;
		bool m_app_start;
		bool m_game_start;
		bool m_end_game;
		bool m_new_game;
		bool network_game;
    };

}


#endif // BS_GAME_H
