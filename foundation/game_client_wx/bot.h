#ifndef _BS_BOT
#define _BS_BOT
#include "board_model.h"
#include <ctime>
namespace battleship
{
class CBot
{
public:
	static CBoardPosition get_move(CBoardPositionContainer & moves)
	{
		srand(time(0));
		unsigned r = 0;
		if(moves.size() == 0)
			r = rand() % moves.size();
		return *(moves.begin() + r);
	}
	
};
}
#endif //_BS_BOT