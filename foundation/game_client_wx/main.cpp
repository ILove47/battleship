#include <iostream>
#include <QApplication>

#include "game.h"
#include "engine.h"
#include "qtengine.h"
#include <ctime>

int main(int argc, char* argv[])
{
	srand(time(0));
    std::cout << "Creating engine.." << std::endl;
	battleship::Engine* engine = new battleship::QtEngine(argc, argv);
    std::cout << "Initializing window.." << std::endl;
    auto window = engine->init_window();
    std::cout << "Creating game.." << std::endl;
	battleship::CGame* game = new battleship::CGame(window->get_input()/*, window->get_canvas()*/);
    std::cout << "Starting game.." << std::endl;
    engine->run(game);
    std::cout << "Finished" << std::endl;
}