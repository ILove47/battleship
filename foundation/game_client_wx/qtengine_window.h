#ifndef CH_QT_ENGINE_WINDOW_H
#define CH_QT_ENGINE_WINDOW_H

#include <QAction>
#include <QMenu>
#include <QLayout>
#include <QWidget>
#include <QtWidgets>
#include <QtWidgets/QMainWindow>
#include <QtGui/QBrush>
#include <QtGui/QPen>
#include <QtGui/QPixmap>
#include <QLabel>

#include "engine_window.h"
#include "qt_canvas2d.h"
#include "ui_game_client.h"
#include "board_view.h"
#include "labels.h"


namespace battleship 
{           
     
    class QtEngineWindow : public QMainWindow, public EngineWindow
    {        
        Q_OBJECT
    public:        
        QtEngineWindow();
        ~QtEngineWindow();

		void update_view(CViewInformation* info);
		bool is_network_game();
	private:
		void update_boards(CViewInformation * info);
		void update_ships(CViewInformation * info);
    private:
        virtual InputSource& do_get_input();
        virtual CCanvas2D& do_get_canvas();
        virtual void do_begin_frame();
        virtual void do_end_frame();

	private:
        void paintEvent(QPaintEvent *event);                           
        void resizeEvent(QResizeEvent *event);
        void mousePressEvent(QMouseEvent *event);      

        void reinit_canvas();
        void reinit_canvasbuffer();
        void delete_objects();
        void create_actions();
        void create_menus();
		void intialize_buttons();
		void create_labels();
		bool is_all_ships_on_board();
    private slots:
        void on_new_game();
        void on_exit_game();
		void on_start_game();
		void on_rand_ships();
		void on_network_game();
    private:
        QPixmap* _canvasbuff;
        QtCanvas2D* _canvas;
        InputSource _input;
        bool _inframe;

        Ui::game_clientClass *ui;
        QMenu* game_menu;
        QAction* newgame_action;
        QAction* exit_action;
		QAction* network_action;
        QLayout* layout;
		QLabel* label;
		QLabel* label_warning;
		QPushButton* start_button;
		QPushButton* rand_button;
		CBoardView* player_board;
		CBoardView* enemy_board;
		CShipViewContainer ships;
		bool network_game;
    };  

}

#endif // CH_QT_ENGINE_WINDOW_H
