#ifndef _BS_CGAME_CLIENT_H
#define _BS_CGAME_CLIENT_H

#include <QAbstractSocket>
#include <QHash>
#include <QHostAddress>

#include "connection_manager.h"

class PeerManager;

class Client : public QObject
{
    Q_OBJECT

public:
    Client();

    void sendMessage(const QString &message);
	void sendInviteMessage();
	void sendAcceptMessage();
	void sendSetShipMessage(const QString &ships);
	void sendMoveMessage(const QString &move);
    QString nickName() const;
    bool hasConnection(const QHostAddress &senderIp, int senderPort = -1) const;
	void setOpponentName(const QString &playerName);
signals:
    void newMessage(const QString &from, const QString &message);
	void newInvite(const QString &from, const QString &sender);
	void newAccept(const QString &from, const QString &sender);
	void getShips(const QString &from, const QString &ships);
	void getMove(const QString &from, const QString &move);
    void newParticipant(const QString &nick);
    void participantLeft(const QString &nick);

private slots:
    void newConnection(Connection *connection);
    void connectionError(QAbstractSocket::SocketError socketError);
    void disconnected();
    void readyForUse();

private:
    void removeConnection(Connection *connection);

    PeerManager *peerManager;
    CConnectionManager server;
    QMultiHash<QHostAddress, Connection *> peers;
	QString opponentName;
};



#endif // _BS_CGAME_CLIENT_H
