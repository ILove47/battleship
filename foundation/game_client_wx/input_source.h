#ifndef BS_INPUT_SOURCE_H
#define BS_INPUT_SOURCE_H
#include <queue>
#include <memory>
#include "board_position.h"
#include "board_model.h"

namespace battleship 
{    
    
    class ControlEvent
    {
    public:        

        enum Type { Type_Invalid, Type_New_Game, Type_Board_Click, Type_Ship_Move, Type_Ship_Rotate, Type_Get_Random_Ships, Type_Start_Game, Type_Start_App  };
		ControlEvent();
		ControlEvent(Type type);
        Type type() const;
    
    private:
        Type _type;
    };
    
	class BoardEvent : public ControlEvent
    {
	public:
		enum BoardType {BoardType_Player_Board, BoardType_Enemy_Board };

		BoardEvent();
		BoardEvent(BoardType& type);

		BoardType type() const;
		void set_type(const BoardType& type);
		void set_pos(const CBoardPosition& pos);
		CBoardPosition get_pos() const;
	private:
		BoardType m_type;
		CBoardPosition m_pos;
    };

	class ShipMoveEvent : public ControlEvent
    {
	public:
		ShipMoveEvent();

		void set_ship(CShip* ship);
		CShip* get_ship()const;
	private:
		CShip* m_ship;
    };
	
	class ShipRotateEvent : public ControlEvent
    {
	public:
		ShipRotateEvent();

		void set_ship(CShip* ship);
		CShip* get_ship()const;
	private:
		CShip* m_ship;
    };
	class GetRandomShipsEvent : public ControlEvent
    {
	public:
		GetRandomShipsEvent(CShipPtrContainer* ships);

		CShipPtrContainer* get_ships()const;
	private:
		CShipPtrContainer* m_ships;
    };

    // class InputSource
    class InputSource
    {
    public:
		typedef std::queue<ControlEvent*> EventsContainer;

        void clear_pending_events();
		ControlEvent* get_event();
        void add_event(ControlEvent* event);
    private:
        EventsContainer m_events;
    };
	
    

}


#endif // BS_INPUT_SOURCE_H
