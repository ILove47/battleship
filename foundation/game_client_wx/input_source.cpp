#include "input_source.h"

namespace battleship
{
    void InputSource::clear_pending_events()
    {
		while (!m_events.empty())
			m_events.pop();
    }
    
    void InputSource::add_event(ControlEvent* event)
    {
		m_events.push(event);
    }
	ControlEvent* InputSource::get_event()
	{
		if (m_events.empty())
		{
			return new ControlEvent();
		}
		ControlEvent* cevent = m_events.front();
		m_events.pop();
		return cevent;
	}
    
	ControlEvent::ControlEvent()
	{
		_type = Type_Invalid;
	}
	ControlEvent::ControlEvent(ControlEvent::Type type)
	{
		_type = type;
	}
    ControlEvent::Type ControlEvent::type() const
    {
        return _type;
    }

	BoardEvent::BoardEvent() : ControlEvent(ControlEvent::Type_Board_Click)
	{
		m_type = BoardType_Player_Board;
	}
	
	BoardEvent::BoardEvent(BoardType& btype) : ControlEvent(ControlEvent::Type_Board_Click)
	{
		m_type = btype;
	}

	void BoardEvent::set_pos(const CBoardPosition& pos)
	{
		m_pos = pos;
	}
	CBoardPosition BoardEvent::get_pos() const
	{
		return m_pos;
	}
	BoardEvent::BoardType BoardEvent::type()const
	{
		return m_type;
	}
	void BoardEvent::set_type(const BoardEvent::BoardType& type)
	{
		m_type = type;
	}
	ShipMoveEvent::ShipMoveEvent() : ControlEvent(ControlEvent::Type_Ship_Move)
	{	
	}

	CShip* ShipMoveEvent::get_ship()const
	{
		return m_ship;
	}
	void ShipMoveEvent::set_ship(CShip* ship)
	{
		m_ship = ship;
	}

	ShipRotateEvent::ShipRotateEvent() : ControlEvent(ControlEvent::Type_Ship_Rotate)
	{	
	}

	CShip* ShipRotateEvent::get_ship()const
	{
		return m_ship;
	}
	void ShipRotateEvent::set_ship(CShip* ship)
	{
		m_ship = ship;
	}

	GetRandomShipsEvent::GetRandomShipsEvent(CShipPtrContainer* ships) : ControlEvent(ControlEvent::Type_Get_Random_Ships), m_ships(ships) {}
	
	CShipPtrContainer* GetRandomShipsEvent::get_ships()const
	{
		return m_ships;
	}
}