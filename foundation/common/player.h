#ifndef _BS_CPLAYER_H
#define _BS_CPLAYER_H
#include <string>
class CPlayer 
{
public:
	CPlayer() : m_name("undefined"){}
	inline std::string get_name() const {return m_name;}
	inline void set_name(const std::string & _name) { m_name = _name; }
private:
	std::string m_name;

};

#endif //  _BS_CPLAYER_H