#ifndef _BS_BOARDMODEL
#define _BS_BOARDMODEL
#include <vector>
#include <map>
#include <cassert>
#include <algorithm>
#include <string>
#include <iterator>
#include <qstring.h>
#include "labels.h"
#include "flags.h"
#include "board_position.h"
namespace battleship 
{
	class CCell
	{
		public:
	        CCell() : m_flags() {} // this is not attacked cell without ship on it.
			
	        //methods that return each state of cell
			inline bool is_empty() const { return !m_flags.is_flag_set(CellFlags_Ship); }
			inline bool is_ship_on() const { return m_flags.is_flag_set(CellFlags_Ship); }
			inline bool is_attacked() const { return m_flags.is_flag_set(CellFlags_Attacked); }
			inline bool is_attacked_ship_on() const { return m_flags.is_flag_set(CellFlags_Attacked | CellFlags_Ship);}
	
	        //methods that set some state of cell             
	        inline void remove_ship() { m_flags.unset_flag(CellFlags_Ship); }
			inline void remove_flags() { m_flags = 0; }
			inline void put_ship_on() { m_flags.set_flag(CellFlags_Ship); }
			inline void attack_cell() { m_flags.set_flag(CellFlags_Attacked); }
	
	        //method return empty cell
			static CCell get_empty_and_not_attacked() { return CCell();} 
		private:
	
	        CCell(int flags) : m_flags(flags){}
	
	        enum CellFlags
	        {
	                CellFlags_Attacked = 0x01,     // indicate attacked (1) or not (0)
	                CellFlags_Ship = 0x02,         // indicate is ship on cell (1) or not (0)
	        };
	
	        CFlags m_flags;
	
	};
	class CShip 
	{
	public:
		enum ShipType
		{
			ShipType_Single_Decker = 1,
			ShipType_Two_Decker = 2,
			ShipType_Three_Decker = 3,
			ShipType_Four_Decker = 4,
		};
		CShip(CBoardPosition start_position,const ShipType& type, bool vertical);
		CShip(const ShipType & type);
		inline CBoardPosition get_start_position() const { return m_start_position;}
		CBoardPosition get_end_position();
		inline void set_start_position( const CBoardPosition & pos ) { m_start_position = pos; }
		inline bool is_vertical() const { return m_vertical;}
		inline ShipType get_ship_type() const { return m_type; }
		bool is_in(const CBoardPosition& pos);
		inline void rotate() { m_vertical ^= 1; }

	private:
		CBoardPosition m_start_position;
		bool m_vertical;
		ShipType m_type;
	};

	typedef std::vector<CCell> CCellsContainer;
	typedef std::vector<CBoardPosition> CBoardPositionContainer;
	typedef std::vector<CShip> CShipContainer;
	typedef std::vector<CShip*> CShipPtrContainer;
	typedef std::map<CShip::ShipType,int> CShipsAmount;
	static CShipsAmount ships_amount = CShipsAmount();

	class CBoardModel
	{
	public:
		CBoardModel();
		void resetBoard();
		const CCell& cell_at(const CBoardPosition& position) const;
		CCell& cell_at(const CBoardPosition& position);
		CCell& cell_at(const size_t& offset);
		const CCell& cell_at(const size_t& offset) const;
		int get_columns_count() const;
		int get_rows_count() const;
		bool is_add_ship_allowed(CShip& ship) const;
		void add_ship(CShip& ship);
		void remove_ship(CShip& ship);
		void get_all_available_moves(CBoardPositionContainer& moves) const;
		void get_all_cells(CCellsContainer& cells) const;
		bool is_move_allowed(const CBoardPosition& move);
		void make_move(const CBoardPosition& move);
		void adjust_missed_cells(const CBoardPosition& move);
		CShipContainer get_randomized_ships();
		void set_randomized_ships();
		bool are_cells_attacked(const CBoardPosition& start, const CBoardPosition& end);
		void attack_adjancent_cells(const CBoardPosition& start, const CBoardPosition& end);
		QString get_all_cells_to_QString();
	private:
		const static int m_rows = 10;
		const static int m_columns = 10;
		const static int all_ships_count = 10;
		CCellsContainer m_board_data;
		CShipContainer m_ships;
		bool is_valid_pos(const CBoardPosition& pos) const;
		bool is_valid_place(CShip& ship) const;
	};
	class CViewInformation
	{
	public:
		CViewInformation(CCellsContainer* board_data_enemy, CCellsContainer* board_data_player, bool update_ships = false, std::pair<bool,std::string> new_label = std::make_pair(false,CLables::get_invalid()) ) : m_board_data_player(board_data_player),m_board_data_enemy( board_data_enemy),m_update_ships(update_ships), m_new_label(new_label), m_new_game(false)
		{		}
		inline void set_board_data_player(CCellsContainer* board_data)
		{
			m_board_data_player = board_data;
		}
		inline const CCellsContainer* get_board_data_player() const
		{
			return m_board_data_player;
		}
		inline void set_board_data_enemy(CCellsContainer* board_data)
		{
			m_board_data_enemy = board_data;
		}
		inline void set_new_game(bool b)
		{
			m_new_game = b;
		}
		inline bool is_new_game()const
		{
			return m_new_game;
		}
		inline const CCellsContainer* get_board_data_enemy() const
		{
			return m_board_data_enemy;
		}
		inline int get_rows() const { return m_rows;}
		inline int get_columns() const { return m_columns; }
		inline std::string get_label() const { return m_new_label.second; }
		inline bool is_main_lable_modified() const { return m_new_label.first; }
		inline void set_update_ships(bool value){ m_update_ships = value; }
		inline void set_label(std::pair<bool,std::string> new_label){ m_new_label = new_label; }
		inline bool is_update_ships() const { return m_update_ships; }
	private:
		bool m_update_ships;
		CCellsContainer* m_board_data_player;
		CCellsContainer* m_board_data_enemy;
		std::pair<bool,std::string> m_new_label;
		static const int m_rows = 10;
		static const int m_columns = 10;
		bool m_new_game;
	};
}

#endif