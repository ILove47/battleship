#ifndef _BS_BOARDPOSITION
#define _BS_BOARDPOSITION

#include <climits>
#include <qstring.h>

namespace battleship 
{
	class CBoardPosition
	{ 
		public:
			CBoardPosition() : m_row(k_default_pos), m_col(k_default_pos){}
			CBoardPosition(int row, int column) : m_row(row), m_col(column){}
			~CBoardPosition() {}
			inline bool operator<(const CBoardPosition& other) const
			{
				return m_row < other.get_row() || (m_row == other.get_row() && m_col < other.get_column());
			}
			bool operator == (const CBoardPosition& other) const
			{
				return m_row == other.m_row  && m_row == other.m_row;
			}
			QString getQStringFromPosition()const
			{
				QString res = "";
				int row = m_row;
				int col = m_col;
				while(row)
				{
					res.push_back('0'+row%10);
					row /= 10;
				}
				res.push_back('@');
				while(col)
				{
					res.push_back('0'+col%10);
					col/= 10;
				}
				return res;
			}
			static const int k_default_pos = INT_MAX; //invalid position
			int get_row() const
			{
				return m_row;
			}
			int get_column() const
			{
				return m_col;
			}
			void set_row(int row)
			{
				m_row = row;
			}
			void set_col(int col)
			{
				m_col = col;
			}
		private:
			int m_row;
			int m_col;
	};


    inline bool is_valid(const CBoardPosition& boardpos)
    {
        return boardpos.get_row() != CBoardPosition::k_default_pos 
            && boardpos.get_column() != CBoardPosition::k_default_pos;
    }
}

#endif