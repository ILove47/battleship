#ifndef _BS_FLAGS
#define _BS_FLAGS

namespace battleship
{
	class CFlags
	{
	
	public:
	        CFlags(): m_flags(0)
			{}
	        CFlags(int flags) : m_flags(flags)
	        {}
			inline bool is_flag_set(int flag) const { return (bool)(m_flags & flag); }
			inline void set_flag(int flag) { m_flags |= flag; }
			inline void unset_flag(int flag) { m_flags &= ~flag; }
			inline void reset() { m_flags = 0; }
	private:
	        int m_flags;
	};
}

#endif _BS_FLAGS