#include "board_model.h"
using namespace battleship;

CShip::CShip(CBoardPosition start_position,const ShipType& type, bool vertical) : m_start_position(start_position), m_vertical(false), m_type(type)
{
}
CShip::CShip(const ShipType & type):m_start_position(),m_vertical(true),m_type(type)
{
}
CBoardPosition CShip::get_end_position()
{
	int len = get_ship_type() - 1;
	int row = m_start_position.get_row();
	int col = m_start_position.get_column();
	if (m_vertical)
		row += len;
	else
		col += len;
	return CBoardPosition(row, col);
}
CBoardModel::CBoardModel() : m_board_data(m_rows*m_columns,CCell::get_empty_and_not_attacked())
{
	if(ships_amount.size() == 0)
	{
		ships_amount[CShip::ShipType_Single_Decker] = 4;
		ships_amount[CShip::ShipType_Two_Decker] = 3;
		ships_amount[CShip::ShipType_Three_Decker] = 2;
		ships_amount[CShip::ShipType_Four_Decker] = 1;
	}
}
bool CShip::is_in(const CBoardPosition& pos)
{
	CBoardPosition end = get_end_position();
	bool result = false;
	if (pos.get_row() >= m_start_position.get_row() && pos.get_row() <= end.get_row() &&
		pos.get_column() >= m_start_position.get_column() && pos.get_column() <= end.get_column() )
		result = true;
	return result;
}

void CBoardModel::resetBoard()
{
	for (int i = 0; i < 10; ++i)
	for (int j = 0; j < 10; ++j)
		cell_at(CBoardPosition(i, j)).remove_flags();
}

CShipContainer CBoardModel::get_randomized_ships()
{
	CShipContainer ships;
	for (int i = 1; i < 5; ++i)
		for (int j = 0; j < i; ++j)
			ships.push_back(CShip(CShip::ShipType(5-i)));
	//random shuffle
	int count = ships.size();
	for (int i = 0; i != count; ++i)
	{
		std::swap(ships[i], ships[i + rand()%(count - i)]);
		if (rand() & 1)
			ships[i].rotate();
	}
	std::vector<std::pair<int, int>> positions;
	positions.reserve(100);
	for (int i = 0; i < 10; ++i)
	for (int j = 0; j < 10; ++j)
		positions.push_back(std::make_pair(i,j));
	
	int countPos = positions.size();
	for (int i = 0; i != countPos; ++i)
		std::swap(positions[i], positions[i + rand()%(countPos - i)]);

	int posShip = 0;
	int posPos = 0;
	bool ok = true;
	while (posShip != count)
	{
		if (posPos == countPos)
		{
			ok = false;
			break;
		}
		ships[posShip].set_start_position(CBoardPosition(positions[posPos].first, positions[posPos].second));
		if (is_add_ship_allowed(ships[posShip]))
			add_ship(ships[posShip++]);
		++posPos;
	}
	if (!ok)
	{
		resetBoard();
		return get_randomized_ships();
	}
	return ships;
}
void CBoardModel::set_randomized_ships()
{
	CShipContainer ships = get_randomized_ships();
	resetBoard();
	for (auto& ship : ships)
	{
		add_ship(ship);
	}
}

int CBoardModel::get_rows_count() const
{
	return m_rows;
}
int CBoardModel::get_columns_count() const
{
	return m_columns;
}
const CCell& CBoardModel::cell_at(const CBoardPosition& position) const
{
	return cell_at(position.get_row() * m_columns + position.get_column());
}
CCell& CBoardModel::cell_at(const CBoardPosition& position)
{
	return cell_at(position.get_row() * m_columns + position.get_column());
}
CCell& CBoardModel::cell_at(const size_t& offset)
{
	return m_board_data.at(offset);
}
const CCell& CBoardModel::cell_at(const size_t& offset) const
{
	return m_board_data.at(offset);
}
bool CBoardModel::is_valid_pos(const CBoardPosition& pos) const
{
	return pos.get_row() >= 0 && pos.get_column() >= 0 &&
		pos.get_row() < m_rows && pos.get_column() < m_columns;
}
bool CBoardModel::is_add_ship_allowed(CShip& ship) const
{
	return is_valid_place(ship);
}
bool CBoardModel::is_valid_place(CShip& ship) const
{
	bool result = true;
	CBoardPosition end_pos = ship.get_start_position();
	int len = ship.get_ship_type() - 1;
	if(ship.is_vertical())
	{
		end_pos = CBoardPosition(end_pos.get_row() + len, end_pos.get_column());
	}
	else
	{
		end_pos = CBoardPosition(end_pos.get_row(), end_pos.get_column()+len);
	}
	if (!is_valid_pos(end_pos))
		result = false;
	
	std::string s[10];
	for (int i = 0; i < 10; ++i)
		s[i] = "";
	for (int i = 0; i < 10; ++i)
	for (int j = 0; j < 10; ++j)
		s[i].push_back(cell_at(CBoardPosition(i,j)).is_ship_on()?'_':'#');
	for(int i = ship.get_start_position().get_row()-1; i <= end_pos.get_row() + 1; i++)
	{
		for(int j = ship.get_start_position().get_column()-1; j <= end_pos.get_column() + 1; j++)
		{
			if( is_valid_pos(CBoardPosition(i,j)) && cell_at(CBoardPosition(i,j)).is_ship_on())
			{
				result = false;
			}
		}
	}

	return result;
}
void CBoardModel::add_ship(CShip& ship)
{
	assert(is_add_ship_allowed(ship));
	m_ships.push_back(ship);
	CBoardPosition end_pos = ship.get_start_position();
	int len = ship.get_ship_type() - 1;
	if(ship.is_vertical())
	{
		end_pos = CBoardPosition(end_pos.get_row() + len, end_pos.get_column());
	}
	else
	{
		end_pos = CBoardPosition(end_pos.get_row(), end_pos.get_column()+len);
	}
	for(int i = ship.get_start_position().get_row(); i <= end_pos.get_row(); ++i)
	{
		for(int j = ship.get_start_position().get_column(); j <= end_pos.get_column(); ++j)
		{
			cell_at(CBoardPosition(i,j)).put_ship_on();
		}
	}
}
void CBoardModel::remove_ship(CShip& ship)
{
	if (!is_valid_pos(ship.get_start_position()))
		return;
	CBoardPosition end_pos = ship.get_start_position();
	int len = ship.get_ship_type() - 1;
	if(ship.is_vertical())
	{
		end_pos = CBoardPosition(end_pos.get_row() + len, end_pos.get_column());
	}
	else
	{
		end_pos = CBoardPosition(end_pos.get_row(), end_pos.get_column()+len);
	}
	for(int i = ship.get_start_position().get_row(); i <= end_pos.get_row(); ++i)
	{
		for(int j = ship.get_start_position().get_column(); j <= end_pos.get_column(); ++j)
		{
			if( is_valid_pos(CBoardPosition(i,j)))
				cell_at(CBoardPosition(i,j)).remove_ship();
		}
	}
}
void CBoardModel::get_all_available_moves(CBoardPositionContainer & moves) const
{
	CBoardPositionContainer moves_colection;

	for(int i = 0; i < get_rows_count(); ++i)
	{
		for(int j = 0; j < get_columns_count(); ++j)
		{
			if(!cell_at(CBoardPosition(i,j)).is_attacked())
			{
				moves_colection.push_back(CBoardPosition(i,j));
			}
		}
	}

	moves.swap(moves_colection);
}
bool CBoardModel::is_move_allowed(const CBoardPosition& move)
{
	return is_valid_pos(move) && !cell_at(move).is_attacked();
}
void CBoardModel::make_move(const CBoardPosition& move)
{
	assert(is_move_allowed(move));
	cell_at(move).attack_cell();
}
bool CBoardModel::are_cells_attacked(const CBoardPosition& start, const CBoardPosition& end)
{
	int rowS = start.get_row();
	int rowE = end.get_row();
	int colS = start.get_column();
	int colE = end.get_column();
	int res = (rowE - rowS + 1) * (colE - colS + 1);
	for (int i = rowS; i <= rowE; ++i)
	for (int j = colS; j <= colE; ++j)
		if (cell_at(CBoardPosition(i, j)).is_attacked())
			--res;
	bool result = true;
	if (res > 0)
		result = false;
	return result;
}
void CBoardModel::attack_adjancent_cells(const CBoardPosition& start, const CBoardPosition& end)
{
	int rowS = start.get_row();
	int rowE = end.get_row();
	int colS = start.get_column();
	int colE = end.get_column();
	
	for (int i = rowS-1; i <= rowE+1; ++i)
	for (int j = colS-1; j <= colE+1; ++j)
	{
		if (is_valid_pos(CBoardPosition(i, j)))
			cell_at(CBoardPosition(i, j)).attack_cell();
	}
}
void CBoardModel::adjust_missed_cells(const CBoardPosition& move)
{
	for (auto& ship : m_ships)
	{
		if (ship.is_in(move))
		{
			if (are_cells_attacked(ship.get_start_position(), ship.get_end_position()))
			{
				attack_adjancent_cells(ship.get_start_position(), ship.get_end_position());
			}
			break;
		}
	}

}
void CBoardModel::get_all_cells(CCellsContainer& cells) const
{
	CCellsContainer data;
	std::copy(m_board_data.begin(),m_board_data.end(),back_inserter(data));
	cells.swap(data);
}
QString CBoardModel::get_all_cells_to_QString()
{
	QString res;
	CCellsContainer::iterator it = m_board_data.begin();
	CCellsContainer::iterator itEnd = m_board_data.end();
	for (; it != itEnd; ++it)
	{
		res.push_back(it->is_ship_on()+'0');
	}
	return res;
}